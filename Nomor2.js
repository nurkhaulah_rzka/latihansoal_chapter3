// This code was made by: @nurkhaulah_rzka

// Diketahui:
// Nilai ujian mahasiswa dikategorikan sebagai berikut :
// ● A : 91 - 100
// ● B : 81 - 90
// ● C : 71 - 80
// ● D : 61 - 70
// ● E : <= 60\

// Pertanyaan:
// Tentukan kategori dari nilai tersebut!


function solutionNo2(n) {
    if (n > 90 && n <= 100) {
      return 'A';
    } else if (n > 80 && n <= 90) {
      return 'B';
    } else if (n > 70 && n <= 80) {
      return 'C';
    } else if (n > 60 && n <= 70) {
      return 'D';
    } else if (n <= 60) {
      return 'E';
    }
  }
  
console.log ("Jika nilai ujian mahasiswa adalah 92, maka kategori nilainya yaitu: " + solutionNo2(92))

console.log ("Jika nilai ujian mahasiswa adalah 87, maka kategori nilainya yaitu: " + solutionNo2(87))

console.log ("Jika nilai ujian mahasiswa adalah 75, maka kategori nilainya yaitu: " + solutionNo2(75))

console.log ("Jika nilai ujian mahasiswa adalah 69, maka kategori nilainya yaitu: " + solutionNo2(69))

console.log ("Jika nilai ujian mahasiswa adalah 43, maka kategori nilainya yaitu: " + solutionNo2(43))