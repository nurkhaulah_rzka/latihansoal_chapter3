// This code was made by: @nurkhaulah_rzka

// Diketahui:
// x = nominal uang yang akan ditarik Yudi, x harus kelipatan 5
// y = jumlah uang (saldo) dalam rekening Yudi
// setiap transaksi berhasil, terkena biaya admin 0.5 rupiah

// Pertanyaan:
// Berapa saldo akun Yudi setelah melakukan transaksi? => return saldo setelah melakukan transaksi

function solutionNo1(x, y) {
    if (x % 5 == 0 && y >= x) {
        y = y - (x + 0.5);
    }
    return y;
}
  
console.log(solutionNo1(75, 340));
console.log(solutionNo1(50, 200));
// console.log(solutionNo1(63, 120)); --> karena x bukan kelipatan 5, sehingga output yang keluar adalah nilai dari y
  