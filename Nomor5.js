// This code was made by: @nurkhaulah_rzka

// -- SOAL: --
// Diberikan sebuah string s.
// Tentukan apakah terdapat karakter `a` dan `b`
// yang jarak `a ke b` atau `b ke a` yang memiliki jarak minimal 3 karakter.

// -- HINT: --
// Check tiap karakter kemudian check 4 karakter setelahnya.

function solutionNo5(str) {
    if (str.includes('a' || 'b')) {
      let splitStr = str.split('');
  
      let index_A = splitStr.indexOf('a');
      // console.log(index_A);
      let index_B = splitStr.indexOf('b');
      // console.log(index_B);
  
      if ((index_B - index_A) > 3) {
        return "YES";
      } else {
        return "NO";
      }
    } else {
      return "Maaf, pada string yang Anda masukkan tidak terdapat karakter a dan b!";
    }
  }
  
  console.log("Jika string inputan: 'acdebae', maka outputnya = " + solutionNo5("acdebae")); // expected output YES
  console.log("Jika string inputan: 'cdaecba', maka outputnya = " + solutionNo5("cdaecba")); // expected output NO
  console.log("Jika string inputan: 'qwerty', maka outputnya = " + solutionNo5("qwerty"));