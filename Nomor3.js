// This code was made by: @nurkhaulah_rzka

// Soal:
// Diberikan sebuah string alamat IP yang valid. Untuk setiap tanda . pada alamat IP gantilah menjadi tanda ()


function solutionNo3(str) {
    var newStr = str.split('');
    for (var i = 0; i < newStr.length; i++) {
        if (newStr[i] === '.') {
        newStr[i] = '()';
        }
    }
    return newStr.join('');
}

console.log(solutionNo3('1.1.1.1')); // output: 1()1()1()1
console.log(solutionNo3('192.168.1.1')); // output: 192()168()1()1